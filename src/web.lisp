(in-package :cl-user)
(defpackage timestamp-microservice.web
  (:use :cl
        :caveman2
        :timestamp-microservice.config
        :timestamp-microservice.view
        :timestamp-microservice.db
        :datafly
        :sxql
        :local-time
        :cl-ppcre)
  (:export :*web*))
(in-package :timestamp-microservice.web)

;; for @route annotation
(syntax:use-syntax :annot)

;;
;; Application

(defclass <web> (<app>) ())
(defvar *web* (make-instance '<web>))
(clear-routing-rules *web*)

;;
;; Routing rules

(defroute "/" ()
  (render #P"index.html"))

(defroute ("/api/timestamp/:date-string") (&key date-string)
  (let* ((unix-date (parse-timestring date-string))
         (utc-date (format-timestring t unix-date)))
    (if (scan "^[0-9]{4}-[0-9]{2}-[0-9]{2}$" date-string)
        (render-json `(unix ,unix-date utc ,utc-date))
        (render-json '(error "Invalid Date")))))

(defroute ("/api/timestamp/?" :regexp t) ()
  (let* ((unix-now (timestamp-to-unix (now)))
         (utc-now (format-timestring t (now))))
    (render-json `(unix, unix-now utc ,utc-now))))

;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
