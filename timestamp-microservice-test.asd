(defsystem "timestamp-microservice-test"
  :defsystem-depends-on ("prove-asdf")
  :author ""
  :license ""
  :depends-on ("timestamp-microservice"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "timestamp-microservice"))))
  :description "Test system for timestamp-microservice"
  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
